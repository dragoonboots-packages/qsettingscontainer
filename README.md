QSettingsContainer
==================


[![pipeline status](https://gitlab.com/dragoonboots-packages/qsettingscontainer/badges/master/pipeline.svg)](https://gitlab.com/dragoonboots-packages/qsettingscontainer/-/commits/master)

**QSettingsContainer** is a header-only library designed to simplify interacting
with [QSettings](https://doc.qt.io/qt-5/qsettings.html) storage.  Instead of
using error-prone string constants and casting the result, a specific API is
defined for stored settings.

Documentation is available at https://dragoonboots-packages.gitlab.io/qsettingscontainer.

See the [installation guide](https://dragoonboots-packages.gitlab.io/qsettingscontainer/usage.html).

Licensed under the [Boost Software License 1.0](https://www.boost.org/LICENSE_1_0.txt).
